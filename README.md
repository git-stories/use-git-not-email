# Use Git, not email
A story about why emailing files can be tough on collaborators, and how Git can help.
